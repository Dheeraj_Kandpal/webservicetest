package kelton.com.testapplication;

import android.app.ProgressDialog;
import android.content.res.ObbInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.kelltontech.application.BaseApplication;
import com.kelltontech.ui.activity.BaseActivity;
import com.kelltontech.ui.widget.CustomAlert;
import com.kelltontech.utils.DeviceInfoUtils;
import com.kelltontech.utils.ShareUtils;
import com.kelltontech.utils.ToastUtils;
import com.kelltontech.volley.ext.GsonObjectRequest;
import com.kelltontech.volley.ext.JsonArrayRequest;
import com.kelltontech.volley.ext.JsonObjectRequest;
import com.kelltontech.volley.ext.RequestManager;


import org.json.JSONArray;
import org.json.JSONObject;

import kelton.com.testapplication.network.Jsonreq;

public class UiTest extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ui_test);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
RequestManager.initializeWith(this,null);
        String dev=DeviceInfoUtils.getModelDesc();
        ToastUtils.showToast(this,dev);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        jsonreq();
    }

    @Override
    public void updateUi(boolean status, int action, Object serviceResponse) {

    }

    @Override
    public void onEvent(int eventId, Object eventData) {

    }

    @Override
    public void getData(int actionID) {

    }
public void jsonreq()
{

  final  ProgressDialog pDialog = new ProgressDialog(this);
    pDialog.setMessage("Loading...");
    pDialog.show();


    RequestManager.addRequest(new JsonArrayRequest("http://api.androidhive.info/mail/outbox.json", new JSONArray(), new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            pDialog.hide();
ToastUtils.showToast(getApplicationContext(),error+"");
           ;
        }
    }){
        @Override
        protected void deliverResponse(JSONArray response) {
            pDialog.hide();
ToastUtils.showToast(getApplicationContext(),response+"");

        }
    });

}
}
