package kelton.com.testapplication.network;

import android.util.Log;

import com.android.volley.Response;
import com.kelltontech.application.BaseApplication;
import com.kelltontech.utils.ToastUtils;
import com.kelltontech.volley.ext.JsonArrayRequest;
import com.kelltontech.volley.ext.JsonObjectRequest;
import com.kelltontech.volley.ext.JsonRequest;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by dheeraj.kandpal on 08-09-2016.
 */
public class Jsonreq extends JsonArrayRequest {
    /**
     * Creates a new request.
     *
     * @param url           URL to fetch the JSON from
     * @param jsonPayload   Listener to receive the JSON response
     * @param errorListener Error listener, or null to ignore errors.
     */
    public Jsonreq(String url, JSONArray jsonPayload, Response.ErrorListener errorListener) {
        super(url, jsonPayload, errorListener);
    }

    @Override
    protected void deliverResponse(JSONArray response) {
        ToastUtils.showToast(BaseApplication.getInstance(),""+response);
    }
}

