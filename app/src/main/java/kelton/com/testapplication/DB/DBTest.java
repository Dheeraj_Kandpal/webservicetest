package kelton.com.testapplication.DB;

import android.app.Application;
import android.content.ContentValues;

import com.kelltontech.database.BaseTable;
import com.kelltontech.model.BaseModel;

import java.util.ArrayList;

/**
 * Created by dheeraj.kandpal on 09-09-2016.
 */
public class DBTest extends BaseTable {
    private static final String DB_SNO = "id";
    public static final String DB_CITY_NAME = "cityName";
    private static final String DB_CENTER_LAT = "centerLat";
    private static final String DB_CENTER_LON = "centerLon";
    public static final String CITY_LIST_TABLE = "city_list_table";

    // private static final String DB_CARD_SPENT_LIMIT = "cardSpentLimit";
    private static final String DB_SPENT_MONEY = "spentMoney";

    public static final String CREATE_CITIES_LIST_TABLE = "create table " + CITY_LIST_TABLE + "(" + DB_CITY_NAME + " VARCHAR PRIMARYKEY not null, "
	/* + DB_CARD_SPENT_LIMIT + " DOUBLE, " */
            + DB_CENTER_LAT + " VARCHAR, " + DB_CENTER_LON + " VARCHAR " + "  )";

    public static final String DROP_QUERY = "Drop table if exists " + CITY_LIST_TABLE;
    public DBTest(Application pApplication, String pTableName) {
        super(pApplication, pTableName);
    }

    @Override
    protected ContentValues getContentValues(BaseModel pModel, boolean onlyUpdates) {
        return null;
    }

    @Override
    protected ArrayList<BaseModel> getAllData(String pSelection, String[] pSelectionArgs) {
        return null;
    }
}
